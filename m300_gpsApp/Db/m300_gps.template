#############################################################################
# Db file for LANTIME M300/GPS, NTP Server                                  #
#                                                                           #
# author: Tomasz Brys                                                       #
#         tomasz.brys@ess.eu                                                #
#                                                                           #
# last modification: 2021-05-27                                             #
#                                                                           #
#                                                                           #
# The Db implements only a few features for monitoring status of the device #
# For more feaures see the documentationa                                   #
#                                                                           #
#############################################################################


#############################################################################
# state of GPS refclock conneted to NTP time server as value. 
# mbgLtNgRefclockState (.1.3.6.1.4.1.5597.30.0.1.2.1.4.1)
# Possible return values:
#    INTEGER {notAvailable(0),synchronized(1),notSynchronized(2)}

record(longin, "$(P)$(R)RefclockState"){
  field(DESC, "Status of GPS refclock")
  field(DTYP, "Snmp")
  field(SCAN, "5 second")
  field(INP,  "@$(HOST) $(COMMUNITY) $(MIB)::mbgLtNgRefclockState.1 ( 100 i")
  field(FLNK, "$(P)$(R)RefclockStateStr")
}

# conversion numbers to string
record(mbbi, "$(P)$(R)RefclockStateStr"){
  field(DESC, "String Status of GPS refclock")
  field(INP,  "$(P)$(R)RefclockState")
  field(ZRST, "Not Available")
  field(ONST, "Synchronized")
  field(TWST, "Not Synchronized")
  field(ZRVL, "0")
  field(ONVL, "1")
  field(TWVL, "2")
}

#############################################################################
# Current status of the NTP service. 
# mbgLtNgNtpCurrentState (.1.3.6.1.4.1.5597.30.0.2.1.0 )
# Possible return values:
#    INTEGER {notAvailable(0),notSynchronized(1),synchronized(2)}

record(longin, "$(P)$(R)NtpCurrentState") {
  field(DESC, "Status of NTP")
  field(DTYP, "Snmp")
  field(SCAN, "5 second")
  field(INP,  "@$(HOST) $(COMMUNITY) $(MIB)::mbgLtNgNtpCurrentState.0 ( 100 i")
  field(FLNK, "$(P)$(R)NtpCurrentStateStr")
}

# conversion numbers to string
record(mbbi, "$(P)$(R)NtpCurrentStateStr"){
  field(DESC, "String Status NTP")
  field(INP,  "$(P)$(R)NtpCurrentState")
  field(ZRST, "Not Available")
  field(ONST, "Not Synchronized")
  field(TWST, "Synchronized")
  field(ZRVL, "0")
  field(ONVL, "1")
  field(TWVL, "2")
}

#############################################################################
# Reference Clock Substate. 
# mbgLtNgRefclockSubstate (.1.3.6.1.4.1.5597.30.0.1.2.1.5 )
# Possible return values:
#    INTEGER {notAvailable(0), gpsSync(1), gpsTracking(2), gpsAntennaDisconnected(3),
#             gpsWarmBoot(4), gpsColdBoot(5), gpsAntennaShortCircuit(6),
#             lwNeverSync(50), lwNotSync(51), lwSync(52), tcrNotSync(100),
#             tcrSync(101), mrsIntOscSync(149), mrsGpsSync(150), mrs10MhzSync(151),
#             mrsPpsInSync(152), mrs10MhzPpsInSync(153), mrsIrigSync(154),
#             mrsNtpSync(155), mrsPtpIeee1588Sync(156), mrsPtpOverE1Sync(157),
#             mrsFixedFreqInSync(158), mrsPpsStringSync(159), mrsVarFreqGpioSync(160),
#             mrsReserved(161), mrsDcf77PzfSync(162), mrsLongwaveSync(163),
#             mrsGlonassGpsSync(164), mrsHavequickSync(165), mrsExtOscSync(166),
#             mrsSyncE(167)}

record(longin, "$(P)$(R)RefclockSubstate") {
  field(DESC, "Substate of GPS")
  field(DTYP, "Snmp")
  field(SCAN, "5 second")
  field(INP,  "@$(HOST) $(COMMUNITY) $(MIB)::mbgLtNgRefclockSubstate.1 ( 100 i")
  field(FLNK, "$(P)$(R)Convert1")
}

# conversion number to string
# aSub is used because mbbi has only 16 inputs, we need here 32 inputs
record(aSub, "$(P)$(R)Convert1"){
  field(DESC, "")
  field(SNAM, "convert1")
  field(INPA, "$(P)$(R)RefclockSubstate")       field(FTA, "LONG") 
  field(OUTA, "$(P)$(R)RefclockSubstateStr PP") field(FTVA,"STRING") field(NOVA,"1")
}

record(stringin, "$(P)$(R)RefclockSubstateStr") {
  field(DESC, "String Substate of GPS")
}

#############################################################################
# Indicates number of good satellites in view
# mbgLtNgNtpCurrentState (.1.3.6.1.4.1.5597.30.0.1.2.1.6.1 )
# Possible return values:
#    Gauge32:  

record(longin, "$(P)$(R)RefclockStatusA") {
  field(DESC, "Nr of good satelites in view")
  field(DTYP, "Snmp")
  field(SCAN, "5 second")
  field(INP,  "@$(HOST) $(COMMUNITY) $(MIB)::mbgLtNgRefclockStatusA.1  Gauge32: 100 i")
}                                         


#############################################################################
# Indicates number of satellites in view
# mbgLtNgNtpCurrentState (.1.3.6.1.4.1.5597.30.0.1.2.1.7.1 )
# Possible return values:
#    Gauge32: 

record(longin, "$(P)$(R)RefclockMaxStatusA") {
  field(DESC, "Nr of satelites in view")
  field(DTYP, "Snmp")
  field(SCAN, "5 second")
  field(INP,  "@$(HOST) $(COMMUNITY) $(MIB)::mbgLtNgRefclockMaxStatusA.1  Gauge32: 100 i")
}

#############################################################################
# Indicates position of the GPS
# mbgLtNgRefclockGpsPos.0 (.1.3.6.1.4.1.5597.30.0.1.5.0 )
# Possible return values:
#    STRING:

record(stringin, "$(P)$(R)RefclockGpsPos") {
  field(DESC, "GPS position")
  field(DTYP, "Snmp")
  field(SCAN, "5 second")
  field(INP,  "@$(HOST) $(COMMUNITY) $(MIB)::mbgLtNgRefclockGpsPos.0  STRING: 100 s")
}


#END
#############################################################################

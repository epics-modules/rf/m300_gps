#include <aSubRecord.h>
#include <registryFunction.h>
#include <epicsExport.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
//=============================================================================
static long convert1(aSubRecord *precord) {
    
    int   code  = *(int*)precord->a;

    switch (code) {
    case 0:
        strcpy((char*)precord->vala, "notAvailable"); 
    break;

    case 1:
        strcpy((char*)precord->vala, "gpsSync"); 
    break;

    case 2:
    strcpy((char*)precord->vala, "gpsTracking"); 
    break;

    case 3:
    strcpy((char*)precord->vala, "gpsAntennaDisconnected");
    break;

    case 4:
    strcpy((char*)precord->vala, "gpsWarmBoot");
    break;

    case 5:
    strcpy((char*)precord->vala, "gpsColdBoot"); 
    break;

    case 6:
    strcpy((char*)precord->vala, "gpsAntennaShortCircuit");
    break;

    case 50:
    strcpy((char*)precord->vala, "lwNeverSync"); 
    break;

    case 51:
    strcpy((char*)precord->vala, "lwNotSync"); 
    break;

    case 52:
    strcpy((char*)precord->vala, "lwSync"); 
    break;

    case 100:
    strcpy((char*)precord->vala, "tcrNotSync");
    break;

    case 101:
    strcpy((char*)precord->vala, "tcrSync"); 
    break;

    case 149:
    strcpy((char*)precord->vala, "mrsIntOscSync"); 
    break;

    case 150:
    strcpy((char*)precord->vala, "mrsGpsSync"); 
    break;

    case 151:
    strcpy((char*)precord->vala, "mrs10MhzSync");
    break;

    case 152:
    strcpy((char*)precord->vala, "mrsPpsInSync");
    break;

    case 153:
    strcpy((char*)precord->vala, "mrs10MhzPpsInSync"); 
    break;

    case 154:
    strcpy((char*)precord->vala, "mrsIrigSync");
    break;

    case 155:
    strcpy((char*)precord->vala, "mrsNtpSync"); 
    break;

    case 156:
    strcpy((char*)precord->vala, "mrsPtpIeee1588Sync");
    break;

    case 157:
    strcpy((char*)precord->vala, "mrsPtpOverE1Sync");
    break;

    case 158:
    strcpy((char*)precord->vala, "mrsFixedFreqInSync"); 
    break;

    case 159:
    strcpy((char*)precord->vala, "mrsPpsStringSync"); 
    break;

    case 160:
    strcpy((char*)precord->vala, "mrsVarFreqGpioSync");
    break;

    case 161:
    strcpy((char*)precord->vala, "mrsReserved"); 
    break;

    case 162:
    strcpy((char*)precord->vala, "mrsDcf77PzfSync"); 
    break;

    case 163:
    strcpy((char*)precord->vala, "mrsLongwaveSync");
    break;

    case 164:
    strcpy((char*)precord->vala, "mrsGlonassGpsSync"); 
    break;

    case 165:
    strcpy((char*)precord->vala, "mrsHavequickSync"); 
    break;

    case 166:
    strcpy((char*)precord->vala, "mrsExtOscSync");
    break;

    case 167:
    strcpy((char*)precord->vala, "mrsSyncE");
    break;

    default:
    strcpy((char*)precord->vala, "Code not supported");
    break;
    }


return 0;
}


epicsRegisterFunction(convert1);

